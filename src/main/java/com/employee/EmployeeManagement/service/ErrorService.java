package com.employee.EmployeeManagement.service;

import org.springframework.stereotype.Service;

@Service
public class ErrorService {
    private String message;

    public ErrorService() {
    }

    public ErrorService(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String displayMessage(String message){
        return message;
    }
}
