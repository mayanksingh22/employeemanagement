package com.employee.EmployeeManagement.service;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class AuditAware implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor()
    {
//        For Spring Security we can add current logged-in user
        return Optional.of("Admin");
    }
}
