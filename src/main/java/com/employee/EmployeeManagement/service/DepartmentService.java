package com.employee.EmployeeManagement.service;

import com.employee.EmployeeManagement.entity.DepartmentEntity;
import com.employee.EmployeeManagement.repository.DepartmentRepository;
import com.employee.EmployeeManagement.request.CreateDepartmentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService {

    @Autowired
    DepartmentRepository departmentRepository;

    public DepartmentEntity createDepartment(CreateDepartmentRequest createDepartmentRequest)
    {
        DepartmentEntity department = new DepartmentEntity();
        department.setName(createDepartmentRequest.getDepartmentName());
        department = departmentRepository.save(department);
        return department;
    }

    public List<DepartmentEntity> getAllDepartments() { return departmentRepository.findAll(); }
}
