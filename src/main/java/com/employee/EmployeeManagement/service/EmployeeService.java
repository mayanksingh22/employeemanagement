package com.employee.EmployeeManagement.service;

import com.employee.EmployeeManagement.entity.EmployeeEntity;
import com.employee.EmployeeManagement.repository.DepartmentRepository;
import com.employee.EmployeeManagement.repository.EmployeeRepository;
import com.employee.EmployeeManagement.request.CreateEmployeeRequest;
import com.employee.EmployeeManagement.request.UpdateEmployeeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    public EmployeeEntity createEmployee(CreateEmployeeRequest createEmployeeRequest)
    {
        EmployeeEntity employee = new EmployeeEntity(createEmployeeRequest);
        if(departmentRepository.getByName(createEmployeeRequest.getD_name()) != null){
            employee.setDepartment(departmentRepository.getByName(createEmployeeRequest.getD_name()));
            employee = employeeRepository.save(employee);
        }
        else {
            return null;
        }
        return employee;
    }

    @Cacheable(cacheNames = "employees")
    public List<EmployeeEntity> getAllEmployee()
    {
        return employeeRepository.findAll();
    }

    @CachePut(cacheNames = "employees", key = "#updateEmployeeRequest.getEid()")
    public EmployeeEntity updateEmployee(@Valid @RequestBody UpdateEmployeeRequest updateEmployeeRequest)
    {
        EmployeeEntity employee = employeeRepository.getById(updateEmployeeRequest.getEid());
        if(updateEmployeeRequest.getEname() != null && !updateEmployeeRequest.getEname().isEmpty())
        {
            employee.setEname(updateEmployeeRequest.getEname());
        }
        if(updateEmployeeRequest.getSalary() > employee.getSalary())
        {
            employee.setSalary(updateEmployeeRequest.getSalary());
        }
        if(updateEmployeeRequest.getDesignation() != null && !updateEmployeeRequest.getDesignation().isEmpty())
        {
            employee.setDesignation(updateEmployeeRequest.getDesignation());
        }
        if(updateEmployeeRequest.getD_name() != null && !updateEmployeeRequest.getD_name().isEmpty())
        {
            if(departmentRepository.getByName(updateEmployeeRequest.getD_name()) != null)
            {
                employee.setDepartment(departmentRepository.getByName(updateEmployeeRequest.getD_name()));
            }
            else{
                return null;
            }
        }

        return employeeRepository.save(employee);
    }

    @CacheEvict(cacheNames = "employees", key = "#id")
    public String deleteEmployee(int id)
    {
        employeeRepository.deleteById(id);
        return "Employee deleted Successfully";
    }
}
