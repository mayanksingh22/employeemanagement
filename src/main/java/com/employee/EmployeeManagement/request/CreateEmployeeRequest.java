package com.employee.EmployeeManagement.request;

import com.employee.EmployeeManagement.entity.DepartmentEntity;
import com.employee.EmployeeManagement.entity.EmployeeEntity;

import javax.validation.constraints.NotBlank;

public class CreateEmployeeRequest {

    @NotBlank(message = "Name is required")
    private String ename;

    private double salary;
    private String designation;

    @NotBlank(message = "Department nae is required")
    private String d_name;

    public CreateEmployeeRequest() {
    }

    public CreateEmployeeRequest(EmployeeEntity employee)
    {
        this.ename = employee.getEname();
        this.salary = employee.getSalary();
        this.d_name = employee.getDepartment().getName();
    }

    public CreateEmployeeRequest(String ename, double salary, String designation, String d_name) {
        this.ename = ename;
        this.salary = salary;
        this.designation = designation;
        this.d_name = d_name;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getD_name() {
        return d_name;
    }

    public void setD_name(String d_name) {
        this.d_name = d_name;
    }
}
