package com.employee.EmployeeManagement.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UpdateEmployeeRequest {
    @NotNull(message = "Id is required")
    private int eid;
    private String ename;
    private double salary;
    private String designation;
    private String d_name;

    public UpdateEmployeeRequest() {
    }

    public UpdateEmployeeRequest(int eid, String ename, double salary, String designation, String d_name) {
        this.eid = eid;
        this.ename = ename;
        this.salary = salary;
        this.designation = designation;
        this.d_name = d_name;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getD_name() {
        return d_name;
    }

    public void setD_name(String d_name) {
        this.d_name = d_name;
    }

    public int getEid() {
        return eid;
    }

    public void setEid(int eid) {
        this.eid = eid;
    }
}
