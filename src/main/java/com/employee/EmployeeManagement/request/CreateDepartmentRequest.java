package com.employee.EmployeeManagement.request;

import javax.validation.constraints.NotBlank;

public class CreateDepartmentRequest {
    @NotBlank(message = "department name is required")
    private String departmentName;

    public CreateDepartmentRequest() {
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    @Override
    public String toString() {
        return "CreateDepartmentRequest{" +
                "departmentName='" + departmentName + '\'' +
                '}';
    }
}
