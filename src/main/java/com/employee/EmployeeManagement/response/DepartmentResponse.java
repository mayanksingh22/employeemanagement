package com.employee.EmployeeManagement.response;

import com.employee.EmployeeManagement.entity.DepartmentEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

public class DepartmentResponse {

    private int id;
    private String name;
    private Date createdDate;
    private Date lastModifiedDate;

    public DepartmentResponse() {
    }

    public DepartmentResponse(DepartmentEntity department)
    {
        this.id = department.getId();
        this.name = department.getName();
        this.createdDate = department.getCreatedDate();
        this.lastModifiedDate = department.getLastModifiedDate();
    }

    public DepartmentResponse(int id, String name,Date createdDate,Date lastModifiedDate) {
        this.id = id;
        this.name = name;
        this.createdDate = createdDate;
        this.lastModifiedDate = lastModifiedDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
