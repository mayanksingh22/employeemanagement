package com.employee.EmployeeManagement.response;

import com.employee.EmployeeManagement.entity.DepartmentEntity;
import com.employee.EmployeeManagement.entity.EmployeeEntity;

import java.util.Date;

public class EmployeeResponse {

    private int eid;
    private String ename;
    private double salary;
    private String designation;
    private Date createdDate;
    private Date lastModifiedDate;
    private DepartmentEntity department;

    public EmployeeResponse() {
    }

    public EmployeeResponse(EmployeeEntity employee)
    {
        this.eid = employee.getEid();
        this.ename = employee.getEname();
        this.salary = employee.getSalary();
        this.designation = employee.getDesignation();
        this.createdDate = employee.getCreatedDate();
        this.lastModifiedDate = employee.getLastModifiedDate();
        this.department = employee.getDepartment();
    }

    public EmployeeResponse(int eid, String ename, double salary, String designation, Date createdDate, Date lastModifiedDate ,DepartmentEntity department) {
        this.eid = eid;
        this.ename = ename;
        this.salary = salary;
        this.designation = designation;
        this.createdDate = createdDate;
        this.lastModifiedDate = lastModifiedDate;
        this.department = department;
    }

    public int getEid() {
        return eid;
    }

    public void setEid(int eid) {
        this.eid = eid;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
