package com.employee.EmployeeManagement.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(
        name = "department",
        uniqueConstraints = @UniqueConstraint(
                name = "name",
                columnNames = "name"
        )
      )
@Embeddable
public class DepartmentEntity extends Auditing {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    public DepartmentEntity() {
    }

    public DepartmentEntity(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Date getCreatedDate(){
        return this.createdDate;
    }

    public String getLastModified(){
        return this.lastModified;
    }

    public Date getLastModifiedDate()
    {
        return this.lastModifiedDate;
    }

    @Override
    public String toString() {
        return "DepartmentEntity{" +
                "createdDate=" + createdDate +
                ", lastModifiedDate=" + lastModifiedDate +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
