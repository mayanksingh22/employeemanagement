package com.employee.EmployeeManagement.entity;

import com.employee.EmployeeManagement.request.CreateEmployeeRequest;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "employee")
@Embeddable
public class EmployeeEntity extends Auditing {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "eid")
    private int eid;

    @Column(name = "ename")
    private String ename;

    @Column(name = "salary")
    private double salary;

    @Column(name = "designation")
    private String designation;

    @Embedded
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private DepartmentEntity department;

    public EmployeeEntity() {
    }

    public EmployeeEntity(int eid, String ename, double salary, String designation) {
        this.eid = eid;
        this.ename = ename;
        this.salary = salary;
        this.designation = designation;
    }

    public EmployeeEntity(CreateEmployeeRequest createEmployeeRequest)
    {
        this.ename = createEmployeeRequest.getEname();
        this.designation = createEmployeeRequest.getDesignation();
        this.salary = createEmployeeRequest.getSalary();
    }

    public int getEid() {
        return eid;
    }

    public void setEid(int eid) {
        this.eid = eid;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    public Date getCreatedDate(){
        return this.createdDate;
    }

    public Date getLastModifiedDate(){
        return this.lastModifiedDate;
    }
}
