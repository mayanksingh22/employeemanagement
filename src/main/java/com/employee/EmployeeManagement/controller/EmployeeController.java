package com.employee.EmployeeManagement.controller;

import com.employee.EmployeeManagement.entity.EmployeeEntity;
import com.employee.EmployeeManagement.repository.EmployeeRepository;
import com.employee.EmployeeManagement.request.CreateEmployeeRequest;
import com.employee.EmployeeManagement.request.UpdateEmployeeRequest;
import com.employee.EmployeeManagement.response.EmployeeResponse;
import com.employee.EmployeeManagement.service.EmployeeService;
import com.employee.EmployeeManagement.service.ErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    EmployeeRepository employeeRepository;

    @PostMapping("/create")
    public EmployeeResponse createEmployee(@Valid @RequestBody CreateEmployeeRequest createEmployeeRequest)
    {
        try{
            EmployeeEntity employee = employeeService.createEmployee(createEmployeeRequest);
            return new EmployeeResponse(employee);
        } catch (NullPointerException e){
            System.out.println("Department not Found");
        }
        return null;
    }

    @GetMapping("/get")
    public List<EmployeeResponse> getAllEmployee()
    {
        List<EmployeeEntity> employeeEntityList = employeeService.getAllEmployee();
        List<EmployeeResponse> employeeResponseList = new ArrayList<EmployeeResponse>();
        employeeEntityList.stream().forEach(employee -> {
            employeeResponseList.add(new EmployeeResponse(employee));
        });
        return employeeResponseList;
    }

    @PutMapping("/update")
    public EmployeeResponse updateEmployee(@Valid @RequestBody UpdateEmployeeRequest updateEmployeeRequest)
    {
        EmployeeEntity employee = employeeService.updateEmployee(updateEmployeeRequest);
        return new EmployeeResponse(employee);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable int id)
    {
        return employeeService.deleteEmployee(id);
    }
}
