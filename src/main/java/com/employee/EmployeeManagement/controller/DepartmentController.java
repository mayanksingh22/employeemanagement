package com.employee.EmployeeManagement.controller;

import com.employee.EmployeeManagement.entity.DepartmentEntity;
import com.employee.EmployeeManagement.request.CreateDepartmentRequest;
import com.employee.EmployeeManagement.response.DepartmentResponse;
import com.employee.EmployeeManagement.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/department")
public class DepartmentController {

    @Autowired
    DepartmentService departmentService;

    @PostMapping("/create")
    public DepartmentResponse createDepartment(@Valid @RequestBody CreateDepartmentRequest createDepartmentRequest)
    {
        DepartmentEntity department = departmentService.createDepartment(createDepartmentRequest);
        return new DepartmentResponse(department);
    }

    @GetMapping("/get")
    public List<DepartmentResponse> getAllDepartments()
    {
        List<DepartmentEntity> departmentEntityList = departmentService.getAllDepartments();
        List<DepartmentResponse> departmentResponseList = new ArrayList<DepartmentResponse>();

        departmentEntityList.stream().forEach(department -> {
            departmentResponseList.add(new DepartmentResponse(department));
        });
        return departmentResponseList;
    }

}
